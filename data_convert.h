#ifndef __H__DATA_CONV__H__
#define __H__DATA_CONV__H__

void __btoa(char *dest, int length, unsigned long long int src);
unsigned long long int __atob(char *src);

void __octtoa(char *dest, int length, unsigned long long int src);
unsigned long long int __atooct(char *src);

void __htoa(char *dest, int length, unsigned long long int src);
unsigned long long int __atoh(char *src);

#endif
