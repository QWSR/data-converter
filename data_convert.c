#include "ctype.h"
#include "limits.h"

#define NUM_OFFSET '0'

#define UPPERCASE_HEX_OFFSET 0x37
#define LOWERCASE_HEX_OFFSET 0x57

unsigned long long int __atob(char *src) {
	unsigned long long int result = 0;

	while(isdigit(*src)) {
		result = result << 1;

		if(*src=='1') {
			++result;
		}

		++src;
	}

	return result;
}

void __btoa(char *dest, int length, unsigned long long int src) {
	unsigned int a_index = 0;

	if(src == 0) {
		while(length > 1) {
			*dest = '0';
			--length;
			++dest;
		}

		*(dest+1)= '\0';

		return;
	}

	while(src > 0) {
		unsigned char rem = src % 2;
		*(dest + a_index) = rem + NUM_OFFSET;

		--length;
		++a_index;
		src /= 2;
	}

	while(length > 1) {
		*(dest + a_index) = '0';
		++a_index;
		--length;
	}

	*(dest + a_index) = '\0';
	--a_index;

	for(unsigned int i=0; i<a_index; ++i) {
		char temp = *(dest + i);
		*(dest + i) = *(dest + a_index);
		*(dest + a_index) = temp;
		
		--a_index;
	}

}

unsigned long long int __atooct(char *src) {
	unsigned long long int result = 0;
	
	while(isdigit(*src)) {
		result = result << 3;
		result += *src - NUM_OFFSET;

		++src;
	}

	return result;
}

void __octtoa(char *dest, int length, unsigned long long int src) {
	unsigned int a_index = 0;

	if(src == 0) {
		while(length > 1) {
			*dest = '0';
			--length;
			++dest;
		}

		*(dest+1)='\0';

		return;
	}

	while(src > 0) {
		unsigned char rem = src % 010;
		*(dest + a_index) = rem + NUM_OFFSET;

		src /= 010;
		--length;
		++a_index;
	}

	while(length > 1) {
		*(dest + a_index) = '0';
		--length;
		++a_index;
	}

	*(dest + a_index) = '\0';
	--a_index;

	for(unsigned int i=0; i<a_index; ++i) {
		char temp = *(dest + i);
		*(dest + i) = *(dest + a_index);
		*(dest + a_index) = temp;

		--a_index;
	}
}

unsigned long long int __atoh(char *src) {
	unsigned long long int result = 0;

	while(isalnum(*src)) {
		if(*src >='a' && *src <='f') {
			result = result << 4;
			result += (*src - LOWERCASE_HEX_OFFSET);
		} else if(*src >='A' && *src <='F') {
			result = result << 4;
			result += (*src - UPPERCASE_HEX_OFFSET);
		} else if(isdigit(*src)) {
			result = result << 4;
			result += (*src - NUM_OFFSET);
		} else {
			return 0;
		}

		++src;
	}

	return result;
}

void __htoa(char *dest, int length, unsigned long long int src) {
	unsigned int a_index = 0;

	if(src==0) {
		while(length > 1) {
			*dest = '0';
			++dest;
			--length;
		}

		*dest='\0';

		return;
	}

	while(src>0) {
		unsigned char rem = src % 0x10;

		if(rem > 0x9) {
			*(dest + a_index) = rem + UPPERCASE_HEX_OFFSET;
		} else {
			*(dest + a_index) = rem + NUM_OFFSET;

		}
		
		--length;
		++a_index;
		src /=0x10;
	}

	while(length > 1) {
		*(dest + a_index) = '0';
		++a_index;
		--length;
	}

	*(dest + a_index) = '\0';
	--a_index;

	for(unsigned int i=0; i < a_index; ++i) {
		char temp = *(dest + i);
		*(dest + i) = *(dest + a_index);
		*(dest + a_index) = temp;

		--a_index;
	}
}
