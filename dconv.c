#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include "ctype.h"

#include "getopt.h"
#include "unistd.h"

#include "data_convert.h"

//constants
#define	ASCII_FORMAT		0
#define BINARY_FORMAT		1
#define OCTAL_FORMAT		2
#define HEXDECIMAL_FORMAT	3

//struct 
typedef struct __file_list{
	FILE *input_file;
	struct __file_list *next;
} file_list;

typedef struct {
	file_list* input_file_begin;
	file_list* input_file_end;

	FILE *input_file;

	int input_format;
	int output_format;
} __option;

//global
char *help_msg = "Usage: dconv [OPTION] [FILE]";
__option runtime_options =	{	NULL,
					NULL,
					NULL,
					ASCII_FORMAT,
					ASCII_FORMAT
				};

//prototypes
int strcaseeq(char *str1, char *str2);
int open_file(char *filename);
int file_init(void);
void cleanup(void);
void raw_output(void);
void ato_convert(void);
void toa_convert(void);

int main(int argc, char **argv) {
	runtime_options.input_file = stdin;

	char **arg_index = argv + 1;

	int option_index = 0;
	int option_result;

	struct option long_options[] = {
		{"help",	no_argument,		0,	'h'},
		{"iformat",	required_argument,	0,	'i'},
		{"oformat",	required_argument,	0,	'o'},
		{NULL,		0,			NULL,	0}
	};

	while( (option_result=getopt_long(argc, argv, "hi:o:", long_options, &option_index)) != -1) {
		switch(option_result) {
			case 'h':
				puts(help_msg);
				return 0;
				break;
			case 'i':
				if(strcaseeq(optarg, "ascii")) {
					runtime_options.input_format = ASCII_FORMAT;
				} else if(/*strcaseeq(optarg, "binary") ||*/ (*optarg=='b')) {
					runtime_options.input_format = BINARY_FORMAT;
				} else if(/*strcaseeq(optarg, "octal") ||*/ (*optarg=='o')) {
					runtime_options.input_format = OCTAL_FORMAT;
				} else if(/*strcaseeq(optarg, "hexdecimal") ||*/ (*optarg=='h')) {
					runtime_options.input_format = HEXDECIMAL_FORMAT;
				} else {
					printf("invalid input format: %s\n", optarg);
					puts(help_msg);
					return 0;
				}

				break;
			case 'o':
				if(strcaseeq(optarg, "ascii")) {
					runtime_options.output_format = ASCII_FORMAT;
				} else if(/*strcaseeq(optarg, "binary") ||*/ (*optarg=='b')) {
					runtime_options.output_format = BINARY_FORMAT;
				} else if(/*strcaseeq(optarg, "octal") ||*/ (*optarg=='o')) {
					runtime_options.output_format = OCTAL_FORMAT;
				} else if(/*strcaseeq(optarg, "hexdecimal") ||*/ (*optarg=='h')) {
					runtime_options.output_format = HEXDECIMAL_FORMAT;
				} else {
					printf("invalid output format: %s\n", optarg);
					puts(help_msg);
					return 0;
				}

				break;
		}
	}

	while(*arg_index!=NULL) {
		if(**arg_index!='-') {
			open_file(*arg_index);
		}

		++arg_index;
	}

	if(runtime_options.input_format==ASCII_FORMAT && runtime_options.output_format==ASCII_FORMAT) {
		if(runtime_options.input_file_begin!=NULL) {
			while(file_init()) {
				raw_output();
			}
		} else {
			raw_output();
		}
	} else if(runtime_options.input_format == ASCII_FORMAT) {
		switch(runtime_options.output_format) {
			case BINARY_FORMAT:
				ato_convert();
				break;
			case OCTAL_FORMAT:
				ato_convert();
				break;
			case HEXDECIMAL_FORMAT:
				ato_convert();
				break;
		}
	} else if(runtime_options.output_format == ASCII_FORMAT) {
		switch(runtime_options.input_format) {
			case BINARY_FORMAT:
				toa_convert();
				break;
			case OCTAL_FORMAT:
				toa_convert();
				break;
			case HEXDECIMAL_FORMAT:
				toa_convert();
				break;
		}
	} else {
		if(runtime_options.input_file_begin!=NULL) {
			while(file_init()) {
				raw_output();
			}
		} else {
			raw_output();
		}
	}

	atexit(cleanup);

	return 0;
}

//functions
int strcaseeq(char *str1, char *str2) {
	while(*str1!='\0' && *str2!='\0') {
		if(tolower(*str1)!=tolower(*str2)) {
			return 0;
		} 

		++str1;
		++str2;
	}

	if(*str1=='\0' && *str2=='\0') {
		return 1;
	} else {
		return 0;
	}
}

int open_file(char *filename) {
	int success = 0;

	if(!access(filename, R_OK)) {

		if(runtime_options.input_file_end==NULL) {
			runtime_options.input_file_begin = runtime_options.input_file_end = calloc(1, sizeof(file_list));
			runtime_options.input_file_end->input_file = fopen(filename, "r");
			runtime_options.input_file_end->next = NULL;
		} else {
			runtime_options.input_file_end->next = calloc(1, sizeof(file_list));
			runtime_options.input_file_end->next->input_file = fopen(filename, "r");
			runtime_options.input_file_end->next->next = NULL;
			runtime_options.input_file_end = runtime_options.input_file_end->next;
		}

		success = 1;
	}

	return success;
}

void cleanup(void) {
	file_list *file_index = runtime_options.input_file_begin;
	file_list *last_index;

	if(runtime_options.input_file!=stdin && runtime_options.input_file!=NULL) {
		fclose(runtime_options.input_file);
	}

	while(file_index) {
		last_index = file_index;
		file_index = file_index->next;
		fclose(last_index->input_file);

		free(last_index);
	}
}

int file_init(void) {
	int success = 1;

	if(runtime_options.input_file_begin!=NULL) {
		if(runtime_options.input_file!=stdin && runtime_options.input_file!=NULL) fclose(runtime_options.input_file);

		file_list *file_index = runtime_options.input_file_begin;
		runtime_options.input_file = runtime_options.input_file_begin->input_file;
		runtime_options.input_file_begin = runtime_options.input_file_begin->next;
		free(file_index);

		return success;
	}

	return 0;
}


char* ascii_init(int format) {
	char *dest = NULL;

	switch(format) {
		case BINARY_FORMAT:
			dest = calloc(9, sizeof(char));
			*(dest+8)='\0';
			break;
		case OCTAL_FORMAT:
			dest = calloc(4, sizeof(char));
			*(dest+3)='\0';
			break;
		case HEXDECIMAL_FORMAT:
			dest = calloc(3, sizeof(char));
			*(dest+2)='\0';
			break;
	}

	return dest;
}

void raw_output(void) {
	int c;

	while((c=fgetc(runtime_options.input_file))!=EOF) {
		putchar(c);
	}
}

char* getstr(char *str, int n) {
	int str_index = 0;
	int successful = 0;
	int c;

	while(isspace(c=fgetc(runtime_options.input_file)));

	if(c==EOF) {
		return NULL;
	} else if(isalnum(c) && n > 1) {
		*(str + str_index) = c;
		++str_index;
		--n;

		successful = 1;
	}

	while((c=fgetc(runtime_options.input_file))!=EOF && isalnum(c) && (n>1)) {
		*(str + str_index) = c;
		++str_index;
		--n;
	}

	if(n >=1) *(str + str_index) = '\0';

	return (successful) ? (str) : (NULL);
}

void ato_convert(void) {
	char *str = ascii_init(runtime_options.output_format);
	int result;
	int length = 3;

	switch(runtime_options.output_format) {
		case BINARY_FORMAT:
			length = 9;
			break;
		case OCTAL_FORMAT:
			length = 4;
			break;
		case HEXDECIMAL_FORMAT:
			length = 3;
			break;
	}

	if(runtime_options.input_file_begin!=NULL) {
		while(file_init()) {
			while(getstr(str, length)) {
				switch(runtime_options.output_format) {
					case BINARY_FORMAT:
						result = __atob(str);
						putchar(result);
						break;
					case OCTAL_FORMAT:
						result = __atooct(str);
						putchar(result);
						break;
					case HEXDECIMAL_FORMAT:
						result = __atoh(str);
						putchar(result);
						break;
				}
			}
		}
	} else {
		while(getstr(str, length)) {
			switch(runtime_options.output_format) {
				case BINARY_FORMAT:
					result = __atob(str);
					putchar(result);
					break;
				case OCTAL_FORMAT:
					result = __atooct(str);
					putchar(result);
					break;
				case HEXDECIMAL_FORMAT:
					result = __atoh(str);
					putchar(result);
					break;
			}
		}
	}

	free(str);
}

void toa_convert(void) {
	char *str = ascii_init(runtime_options.input_format);
	int last_c = EOF;
	int c;

	if(runtime_options.input_file_begin!=NULL) {
		while(file_init()) {
			while((c=fgetc(runtime_options.input_file))!=EOF) {
				if(last_c==EOF) {
					last_c = c;
				} else {
					switch(runtime_options.input_format) {
						case BINARY_FORMAT:
							__btoa(str, 9, last_c);
							break;
						case OCTAL_FORMAT:
							__octtoa(str, 4, last_c);
							break;
						case HEXDECIMAL_FORMAT:
							__htoa(str, 3, last_c);
							break;
					}

					printf(str);
					putchar(' ');
					last_c = c;
				}
			}

			switch(runtime_options.input_format) {
				case BINARY_FORMAT:
					__btoa(str, 9, last_c);
					break;
				case OCTAL_FORMAT:
					__octtoa(str, 4, last_c);
					break;
				case HEXDECIMAL_FORMAT:
					__htoa(str, 3, last_c);
					break;
			}
		}
	} else {
		while((c=fgetc(runtime_options.input_file))!=EOF) {
			if(last_c==EOF) {
				last_c = c;
			} else {
				switch(runtime_options.input_format) {
					case BINARY_FORMAT:
						__btoa(str, 9, last_c);
						break;
					case OCTAL_FORMAT:
						__octtoa(str, 4, last_c);
						break;
					case HEXDECIMAL_FORMAT:
						__htoa(str, 3, last_c);
						break;
				}

				printf(str);
				putchar(' ');
				last_c = c;
			}
		}

		switch(runtime_options.input_format) {
			case BINARY_FORMAT:
				__btoa(str, 9, last_c);
				break;
			case OCTAL_FORMAT:
				__octtoa(str, 4, last_c);
				break;
			case HEXDECIMAL_FORMAT:
				__htoa(str, 3, last_c);
				break;
		}
	}

	printf(str);
	free(str);
}
